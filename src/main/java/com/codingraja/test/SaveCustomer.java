package com.codingraja.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Customer;
import com.codingraja.domain.Order;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("config/hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order("MacBook", "MacBook Air", "Apple", 75000.0));
		orders.add(new Order("Laptop", "G50", "Lenovo", 35000.0));

		Customer customer = new Customer("Coding", "RAJA", "info@codingraja.com", 9742900696L);
		customer.setOrders(orders);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(customer);
		transaction.commit();
		session.close();
		
		System.out.println("Customer Record saved successfully!");
	}
}

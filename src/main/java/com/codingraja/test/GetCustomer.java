package com.codingraja.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Customer;
import com.codingraja.domain.Order;

public class GetCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("config/hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		/*This is bidirectional one-to-many/Many-to-one association, 
		 * so if you have Customer ID then you can fetch Customer Record 
		 * as well as Order's of Customer*/
		Session session = factory.openSession();
		Customer customer = session.get(Customer.class, new Long(1));
		List<Order> orders = customer.getOrders();
		session.close();
		
		System.out.println("Customer Email: "+customer.getEmail());
		System.out.println("Customer Orders: "+orders);
		
		/*if you have Order ID then you can fetch Order Details and
		Customer Record, because this is bidirectional.
		This is called Many-To-One Association*/
		Session session1 = factory.openSession();
		Order order = session1.get(Order.class, new Long(2));
		session1.close();
		
		System.out.println("Item Name: "+order.getBrand());
		System.out.println("Customer Name: "+order.getCustomer().getFirstName());
	}
}

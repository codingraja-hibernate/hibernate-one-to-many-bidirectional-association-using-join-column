package com.codingraja.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="order_master10")
public class Order {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@Column(name="item_name")
	private String itemName;
	@Column(name="model")
	private String model;
	@Column(name="brand")
	private String brand;
	@Column(name="price")
	private Double price;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumns(value=@JoinColumn(name="CUST_ID"))
	private Customer customer;
	
	public Order(){}
	public Order(String itemName, String model, String brand, Double price) {
		super();
		this.itemName = itemName;
		this.model = model;
		this.brand = brand;
		this.price = price;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@Override
	public String toString() {
		return "Order [id=" + id + ", itemName=" + itemName + ", model=" + model + ", brand=" + brand + ", price="
				+ price + ", customer=" + customer + "]";
	}
}
